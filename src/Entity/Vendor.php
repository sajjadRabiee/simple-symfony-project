<?php

namespace App\Entity;

use App\Repository\VendorRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Internal\TentativeType;

#[ORM\Entity(repositoryClass: VendorRepository::class)]
class Vendor implements \JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(name: 'name', type: 'string')]
    private string $name;

    #[ORM\Column(name: 'family', type: 'string')]
    private string $family;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    public function getName() {
        return $this->name;
    }


    public function setFamily(string $family)
    {
        $this->family = $family;
        return $this;
    }

    public function getFamily(): string
    {
        return $this->family;
    }


    public function jsonSerialize(): mixed
    {
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "family" => $this->getFamily()


        ];
    }
}
