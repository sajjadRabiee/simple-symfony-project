<?php

namespace App\Controller;

use App\Repository\VendorRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class VendorController extends BaseController
{

    private VendorRepository $vendorRepository;

    public function __construct(
        VendorRepository $vendorRepository
    )
    {
        $this->vendorRepository = $vendorRepository;
    }

    #[Route('vendors', name: 'vendors_index', methods: ["GET"])]
    public function vendors() {
        $vendors = $this->vendorRepository->findAll();
        return new JsonResponse($vendors);
    }
}