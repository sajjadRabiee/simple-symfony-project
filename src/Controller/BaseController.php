<?php

namespace App\Controller;

use App\Entity\Vendor;
use App\Repository\VendorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{
    #[Route('/', name: 'index')]
    function index(): JsonResponse
    {
        return $this->success();
    }

    protected function success(array $data = []): JsonResponse
    {
        $status = [
            'status' => 'success',
        ];

        return new JsonResponse(array_merge($status, $data));
    }
}